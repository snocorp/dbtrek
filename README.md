# DB Trek

DB Trek is a migration system that tracks migrations in a YAML file so that they can be tracked with your source code changes.

# Migration File

The migration file must be in YAML format as shown in the example below:

```yaml
migrations:
  20180910220000:
    up:
      type: sql
      value: |
        CREATE TABLE users
        (
            id bigint NOT NULL,
            email character varying(255) NOT NULL,
            PRIMARY KEY (id)
        );
    down:
      type: sql
      value: |
        DROP TABLE users;
```

- migrations - a map of keys to objects describing migrations
- up - an object describing the migration
- down - an object describing the reverse migration or rollback
- type - the type of the migration
  - sql - migration or rollback sql is the value
- value - the description of or a reference to the actual migration or rollback logic

Note that migrations are run in the order of the migration keys. It is recommended to use a date-based string for your key but it can be any string.

# Migration File Commands

## Add Migration

Add a new migration to an existing migration file.

```
trek add \
  --migrations ./data/migrations.yaml
```

**Flags**

- migrations - The file describing the migrations
- type - The type of migration, defaults to "sql"
- output - The file to output to, defaults to the migration file

# Database Commands

## Initialization

Initialization is required in order to run migrations. It can either be done beforehand or automatically when the first migration is executed.

Initialization will create a table in your database schema called `dbtrek_migrations` as follows:

```sql
CREATE TABLE dbtrek_migrations
(
    id bigint NOT NULL,
    CONSTRAINT dbtrek_migrations_pkey PRIMARY KEY (id)
)
```

To initialize a schema, run the following command:

```
trek db init \
  --dbconfig postgresql://user:password@hostname:5432/database?sslmode=disable
```

**Flags**

- dbconfig - The connection URI to your database (or use `TREK_DB` environment variable)
- dbtype - The type of database (postgres, mysql), default is postgres

## Migrate

To run all the remaining migrations in your migration file, run the following command:

```
trek db migrate \
  --dbconfig postgresql://user:password@hostname:5432/database?sslmode=disable \
  --migrations ./data/migrations.yaml
```

**Flags**

- dbconfig - The connection URI to your database (or use `TREK_DB` environment variable)
- dbtype - The type of database (postgres, mysql), default is postgres
- migrations - The file describing the migrations (or use `TREK_MIGRATIONS` environment variable)
- steps - The number of steps to migrate, default is all remaining steps

## Rollback

To roll back the most recent migration, run the following command:

```
trek db rollback \
  --dbconfig postgresql://user:password@hostname:5432/database?sslmode=disable \
  --migrations ./data/migrations.yaml
  --steps 1
```

**Flags**

- dbconfig - The connection URI to your database (or use `TREK_DB` environment variable)
- dbtype - The type of database (postgres, mysql)
- migrations - The file describing the migrations (or use `TREK_MIGRATIONS` environment variable)
- steps - The number of steps to migrate, default is 1 step

# Docker Image

The docker image for dbtrek is available at registry.gitlab.com/snocorp/dbtrek. The image tag is the version number or use `latest` for the latest main.
