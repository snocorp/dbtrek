package api

import (
	"io/ioutil"

	"gitlab.com/snocorp/dbtrek/migrations"
	"gopkg.in/yaml.v2"
)

func (t *dbtrek) Add(inputFile, migrationType, outputFile string) (err error) {
	m, err := migrations.LoadMigrationFile(inputFile)
	if err != nil {
		return
	}

	migrations.AddMigration(&m, migrationType, migrationType)

	data, err := yaml.Marshal(m)
	if err != nil {
		return
	}

	err = ioutil.WriteFile(outputFile, data, 0644)
	if err != nil {
		return
	}

	return
}
