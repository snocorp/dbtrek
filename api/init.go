package api

func (t *dbtrek) Init() error {
	return t.migrator.CreateMigrationsTable()
}
