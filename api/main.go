package api

import (
	"database/sql"

	"gitlab.com/snocorp/dbtrek/migrations"
)

// DBTrek is the interface between the UI and the logic.
type DBTrek interface {
	SetDB(db *sql.DB)
	SetMigrator(migrator migrations.Migrator)

	Add(inputFile, migrationType, outputFile string) error
	Init() error
	Migrate(migrateFile string, steps int) error
	Rollback(rollbackFile string, steps int) error
}

type dbtrek struct {
	db       *sql.DB
	migrator migrations.Migrator
}

func (t *dbtrek) SetDB(db *sql.DB) {
	if t.db != nil {
		panic("SetDB can only be called once")
	}

	t.db = db
}

func (t *dbtrek) SetMigrator(migrator migrations.Migrator) {
	if t.migrator != nil {
		panic("SetMigrator can only be called once")
	}

	t.migrator = migrator
}

// NewDBTrek creates a new instance of the DBTrek API.
func NewDBTrek() DBTrek {
	return &dbtrek{}
}
