package api

import (
	"database/sql"
	"fmt"
	"log"
	"sort"

	"gitlab.com/snocorp/dbtrek/migrations"
)

func (t *dbtrek) Migrate(migrateFile string, steps int) (err error) {
	log.Print("Checking dbtrek_migrations table")
	err = t.migrator.SelectMigrationTable(func(rows *sql.Rows, err error) error {
		if err != nil {
			return err
		}
		if !rows.Next() {
			err = t.Init()
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return
	}

	m, err := migrations.LoadMigrationFile(migrateFile)
	if err != nil {
		return
	}

	err = migrations.LoadMigrations(t.migrator, &m)
	if err != nil {
		return
	}

	// gather all the migration keys and sort them
	var allKeys []string
	for id := range m.Migrations {
		allKeys = append(allKeys, id)
	}
	sort.Strings(allKeys)

	// validate that no complete migration follows incomplete migrations
	var keys []string
	for _, id := range allKeys {
		migration := m.Migrations[id]
		if migration.Complete && len(keys) > 0 {
			return fmt.Errorf("migration %v is complete but incomplete migrations precede it", id)
		} else if !migration.Complete {
			keys = append(keys, id)
		}
	}

	// ensure steps is valid
	if steps <= 0 || steps > len(keys) {
		steps = len(keys)
	}

	// loop over the migration up to the number of steps given
	for i := 0; i < steps; i++ {
		id := keys[i]
		log.Printf("Executing migration logic for '%v'", id)
		switch m.Migrations[id].Up.Type {
		case "sql":
			err = t.migrator.RunMigrationSQL(m.Migrations[id].Up.Value)
		default:
			err = fmt.Errorf("Invalid type '%v' for migration '%v'", m.Migrations[id].Up.Type, id)
		}
		if err != nil {
			return
		}

		log.Printf("Marking '%v' complete", id)
		err = t.migrator.MarkMigrationComplete(id)
		if err != nil {
			return
		}
	}

	return
}
