package api

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"sort"

	"gitlab.com/snocorp/dbtrek/migrations"
)

func (t *dbtrek) Rollback(rollbackFile string, steps int) (err error) {
	log.Print("Checking dbtrek_migrations table")
	err = t.migrator.SelectMigrationTable(func(rows *sql.Rows, err error) error {
		if err != nil {
			return err
		}
		if !rows.Next() {
			return errors.New("Database is not intialized and cannot be rolled back")
		}

		return nil
	})
	if err != nil {
		return
	}

	m, err := migrations.LoadMigrationFile(rollbackFile)
	if err != nil {
		return
	}

	err = migrations.LoadMigrations(t.migrator, &m)
	if err != nil {
		return
	}

	// gather all the migration keys and sort them
	var allKeys []string
	for id := range m.Migrations {
		allKeys = append(allKeys, id)
	}
	sort.Sort(sort.Reverse(sort.StringSlice(allKeys)))

	// validate that no complete migration follow incomplete migrations
	var keys []string
	for _, id := range allKeys {
		migration := m.Migrations[id]
		if !migration.Complete && len(keys) > 0 {
			return fmt.Errorf("migration %v is incomplete but complete migrations follow it", id)
		} else if migration.Complete {
			keys = append(keys, id)
		} else {
			log.Print(migration)
		}
	}

	// ensure steps is valid
	if steps <= 0 || steps > len(keys) {
		steps = len(keys)
	}

	// loop over the migration up to the number of steps given
	for i := 0; i < steps; i++ {
		id := keys[i]
		log.Printf("Executing rollback logic for '%v'", id)
		switch m.Migrations[id].Down.Type {
		case "sql":
			err = t.migrator.RunMigrationSQL(m.Migrations[id].Down.Value)
		default:
			err = fmt.Errorf("Invalid type '%v' for migration '%v'", m.Migrations[id].Up.Type, id)
		}
		if err != nil {
			return
		}

		log.Printf("Marking '%v' incomplete", id)
		err = t.migrator.MarkMigrationIncomplete(id)
		if err != nil {
			return
		}
	}

	return
}
