package cmd

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
)

var addFile string
var addType string
var addOutputFile string

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Adds a new migration to the migration file",
	Long:  `Adds a new migration to the migration file`,
	Run: func(cmd *cobra.Command, args []string) {
		if addFile == "" {
			addFile = os.Getenv(envTrekMigrations)
			if addFile == "" {
				runtimeErr = errors.New("migrations flag is required")
				return
			}
		}

		if addOutputFile == "" {
			addOutputFile = addFile
		}

		runtimeErr = dbtrek.Add(addFile, addType, addOutputFile)
	},
}

func init() {
	addCmd.Flags().StringVarP(&addFile, "migrations", "m", "", "(required) The yaml file containing the migrations")
	addCmd.Flags().StringVarP(&addType, "type", "", "sql", "The type for up and down migrations")
	addCmd.Flags().StringVarP(&addOutputFile, "output", "", "", "The file to which the output is written (default uses the input file)")

	rootCmd.AddCommand(addCmd)
}
