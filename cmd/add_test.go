package cmd

import (
	"io/ioutil"
	"log"
	"testing"
	"time"

	"gitlab.com/snocorp/dbtrek/migrations"
	yaml "gopkg.in/yaml.v2"
)

func TestAddCmd(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "TestAddCmd*.yaml")
	if err != nil {
		log.Fatal(err)
	}

	err = executeCmd("add",
		"--migrations", "../examples/mysql/sql.yaml",
		"--output", tmpfile.Name(),
	)
	if err != nil {
		t.Error(err)
		return
	}

	data, err := readFile(addFile)
	if err != nil {
		t.Error(err)
		return
	}

	var m migrations.Migrations
	err = yaml.Unmarshal(data, &m)
	if err != nil {
		t.Error(err)
		return
	}

	for k, v := range m.Migrations {
		if k != "20180910220000" {
			timestamp, err := time.Parse("20060102150405", k)
			if err != nil {
				t.Error(err)
			}

			now := time.Now()
			if timestamp.Add(time.Second * 10).Before(now) {
				t.Errorf("Expected timestamp to be less than 10s before current time but got %v", now.Sub(timestamp))
				return
			}

			if v.Up.Type != "sql" {
				t.Errorf("Expected up type to be 'sql' but got '%v'", v.Up.Type)
			}
			if v.Down.Type != "sql" {
				t.Errorf("Expected down type to be 'sql' but got '%v'", v.Down.Type)
			}
		}
	}
}

func TestAddCmdNoOutput(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "TestAddCmd*.yaml")
	if err != nil {
		log.Fatal(err)
	}

	_, err = copy("../examples/mysql/sql.yaml", tmpfile.Name())
	if err != nil {
		log.Fatal(err)
	}

	err = executeCmd("add",
		"--migrations", tmpfile.Name(),
	)
	if err != nil {
		t.Error(err)
		return
	}

	data, err := readFile(addFile)
	if err != nil {
		t.Error(err)
		return
	}

	var m migrations.Migrations
	err = yaml.Unmarshal(data, &m)
	if err != nil {
		t.Error(err)
		return
	}

	if len(m.Migrations) != 2 {
		t.Errorf("Expected 2 migrations but got %v", len(m.Migrations))
		return
	}
}

func TestAddCmd_MissingMigrationFlag(t *testing.T) {
	err := executeCmd("add",
		"--output", "output.yaml",
	)
	if err == nil {
		t.Errorf("Expected an error but got none")
	} else if err.Error() != "migrations flag is required" {
		t.Error(err)
	}
}
