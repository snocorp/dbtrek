package cmd

import (
	"database/sql"
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/snocorp/dbtrek/migrations"
	"gitlab.com/snocorp/dbtrek/migrations/mysql"
	"gitlab.com/snocorp/dbtrek/migrations/postgres"
)

var dbConfig string
var dbType string
var db *sql.DB

var migrator migrations.Migrator

var dbRootCmd = &cobra.Command{
	Use:   "db",
	Short: "Runs a subcommand on a database schema",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		if dbConfig == "" {
			// fall back to ENV variable
			dbConfig = os.Getenv(envTrekDB)
			if dbConfig == "" {
				return errors.New("dbconfig flag is required")
			}
		}
		if dbType != "postgres" && dbType != "mysql" {
			return errors.New("dbtype must be one of: postgres, mysql")
		}
		db, err = sql.Open(dbType, dbConfig)
		if err != nil {
			return
		}
		dbtrek.SetDB(db)

		migrator, err = newMigrator(dbType, db)
		if err != nil {
			return
		}
		dbtrek.SetMigrator(migrator)

		return
	},
	PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
		db.Close()

		return runtimeErr
	},
}

func init() {
	dbRootCmd.PersistentFlags().StringVar(&dbType, "dbtype", "postgres", "The database type, postgres and mysql are supported values")
	dbRootCmd.PersistentFlags().StringVar(&dbConfig, "dbconfig", "", "(required) The database connection information as a connection URI")

	rootCmd.AddCommand(dbRootCmd)
}

func newMigrator(dbType string, db *sql.DB) (migrations.Migrator, error) {
	if dbType == "postgres" {
		return postgres.NewMigrator(db), nil
	} else if dbType == "mysql" {
		return mysql.NewMigrator(db), nil
	}
	return nil, fmt.Errorf("Invalid dbtype %v", dbType)
}
