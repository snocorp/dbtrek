package cmd

import (
	"database/sql"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/snocorp/dbtrek/common"
	"gitlab.com/snocorp/dbtrek/migrations"
)

var dbtypeList = []string{"postgres", "mysql"}

// var dbtypeList = []string{"mysql"}

// testDb is used to run queries from test functions and is closed at the end of the tests, but before the database is dropped
var testDb *sql.DB

func reset() {
	addFile = ""
	addType = ""
	addOutputFile = ""

	dbConfig = ""

	migrateFile = ""
	migrateSteps = 0

	rollbackFile = ""
	rollbackSteps = 0

	runtimeErr = nil
}

// executeCmd allows tests to execute commands using the provided arguments
func executeCmd(args ...string) error {
	reset()

	log.Printf("Executing command with args: %v\n", strings.Join(args, " "))
	rootCmd.SetArgs(args)
	err := execute()
	if err != nil {
		return err
	}
	return runtimeErr
}

// expectMigrationsTable expects the migrations table to be the one row that is returned
func expectMigrationsTable(t *testing.T, migrator migrations.Migrator) {
	expectOneRow(t, migrator.SelectMigrationTable)
}

// getDbName returns a time-based db name
func getDbName() string {
	return fmt.Sprintf("testdb_%v", time.Now().UnixNano())
}

func getDbVars(dbtype string) (user, password, host string) {
	var ok bool
	upperDbType := strings.ToUpper(dbtype)
	user, ok = os.LookupEnv(fmt.Sprintf("TREK_%v_TEST_USER", upperDbType))
	if !ok {
		user = "root"
	}
	password, ok = os.LookupEnv(fmt.Sprintf("TREK_%v_TEST_PASSWORD", upperDbType))
	if !ok {
		password = "dbtrek"
	}
	host, ok = os.LookupEnv(fmt.Sprintf("TREK_%v_TEST_HOST", upperDbType))
	if !ok {
		host = "localhost"
	}
	return
}

func getAdminDbConnectionURI(dbtype string) string {
	user, password, host := getDbVars(dbtype)
	if dbtype == "postgres" {
		return fmt.Sprintf("postgres://%v:%v@%v:5432/root?sslmode=disable", user, password, host)
	} else if dbtype == "mysql" {
		return fmt.Sprintf("%v:%v@tcp(%v:3306)/?tls=skip-verify&autocommit=true", user, password, host)
	}
	panic(fmt.Sprintf("Unexpected database type %v", dbtype))
}

func getDbConnectionURI(dbtype, dbname string) string {
	user, password, host := getDbVars(dbtype)
	if dbtype == "postgres" {
		return fmt.Sprintf("postgres://%v:%v@%v:5432/%v?sslmode=disable", user, password, host, dbname)
	} else if dbtype == "mysql" {
		return fmt.Sprintf("%v:%v@tcp(%v:3306)/%v?tls=skip-verify&autocommit=true", user, password, host, dbname)
	}
	panic(fmt.Sprintf("Unexpected database type %v", dbtype))
}

// createDatabase cretes a testing database for a test case
func createDatabase(dbtype, dbname string) (drop func(), err error) {
	drop = func() {}
	adminDB, err := sql.Open(dbtype, getAdminDbConnectionURI(dbtype))
	if err != nil {
		return
	}

	_, err = adminDB.Exec(fmt.Sprintf("CREATE DATABASE %v", dbname))
	if err != nil {
		log.Print("Unable to create database for test")
		return
	}
	drop = func() {
		_, err = adminDB.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %v", dbname))
		if err != nil {
			log.Printf("Unable to drop database %v for test", dbname)
		}
		err = adminDB.Close()
		if err != nil {
			log.Printf("Unable to close connection to %v", dbname)
		}
	}

	return
}

// expectOneRow expects only one row to be given in the row handler
func expectOneRow(t *testing.T, selector func(common.RowHandler) error) {
	err := selector(func(rows *sql.Rows, err error) error {
		if err != nil {
			return err
		}
		if !rows.Next() {
			t.Error("Expected 1 row but got no rows")
		}
		if rows.Next() {
			t.Error("Expected 1 row but got more than one")
		}

		return nil
	})
	if err != nil {
		t.Error(err)
	}
}

// expectNoRows expects no rows to be given in the row handler
func expectNoRows(t *testing.T, selector func(common.RowHandler) error) {
	err := selector(func(rows *sql.Rows, err error) error {
		if err != nil {
			return err
		}
		if rows.Next() {
			t.Error("Expected no rows")
		}

		return nil
	})
	if err != nil {
		t.Error(err)
	}
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
