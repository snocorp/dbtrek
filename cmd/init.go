package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	dbRootCmd.AddCommand(initCmd)
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initializes the database schema for DB Trek",
	Long:  `Initializes the database schema for DB Trek`,
	Run: func(cmd *cobra.Command, args []string) {
		runtimeErr = dbtrek.Init()
	},
}
