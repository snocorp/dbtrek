package cmd

import (
	"database/sql"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

func TestInitCmd(t *testing.T) {
	for _, dbtype := range dbtypeList {
		t.Run(dbtype, func(t *testing.T) {
			dbname := getDbName()
			drop, err := createDatabase(dbtype, dbname)
			defer drop()
			if err != nil {
				t.Error(err)
			}

			dbConfigURI := getDbConnectionURI(dbtype, dbname)
			err = executeCmd("db", "init",
				"--dbtype", dbtype,
				"--dbconfig", dbConfigURI,
			)
			if err != nil {
				t.Error(err)
				return
			}

			db, err := sql.Open(dbtype, dbConfigURI)
			if err != nil {
				t.Error(err)
				return
			}
			defer db.Close()

			m, err := newMigrator(dbtype, db)
			if err != nil {
				t.Error(err)
				return
			}
			expectMigrationsTable(t, m)
		})
	}
}

func TestInitCmd_MissingDbconfigFlag(t *testing.T) {
	err := executeCmd("db", "init")
	if err == nil {
		t.Errorf("Expected an error but got none")
	} else if err.Error() != "dbconfig flag is required" {
		t.Error(err)
	}
}

func TestInitCmd_InvalidDbType(t *testing.T) {
	dbConfigURI := getDbConnectionURI("postgres", getDbName())
	err := executeCmd("db", "init",
		"--dbtype", "invalid",
		"--dbconfig", dbConfigURI,
	)
	if err == nil {
		t.Errorf("Expected an error but got none")
	} else if err.Error() != "dbtype must be one of: postgres, mysql" {
		t.Error(err)
	}
}
