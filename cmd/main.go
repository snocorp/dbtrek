package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/snocorp/dbtrek/api"
)

const (
	envTrekDB         = "TREK_DB"
	envTrekMigrations = "TREK_MIGRATIONS"
)

var runtimeErr error

var dbtrek api.DBTrek

func execute() error {
	dbtrek = api.NewDBTrek()

	return rootCmd.Execute()
}

// Execute runs the command
func Execute() {
	if err := execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if runtimeErr != nil {
		fmt.Println(runtimeErr)
		os.Exit(2)
	}
}

func readFile(migrateFile string) ([]byte, error) {
	log.Printf("Loading file %v", migrateFile)
	return ioutil.ReadFile(migrateFile)
}
