package cmd

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
)

var migrateSteps int
var migrateFile string

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Migrates a database schema",
	Long:  `Migrates a database schema the given number of steps defined in the migrations file`,
	Run: func(cmd *cobra.Command, args []string) {
		if migrateFile == "" {
			migrateFile = os.Getenv(envTrekMigrations)
			if migrateFile == "" {
				runtimeErr = errors.New("migrations flag is required")
				return
			}
		}

		runtimeErr = dbtrek.Migrate(migrateFile, migrateSteps)
	},
}

func init() {
	migrateCmd.Flags().StringVarP(&migrateFile, "migrations", "m", "", "(required) The yaml file containing the migrations")
	migrateCmd.Flags().IntVarP(&migrateSteps, "steps", "s", 0, "The number of steps to run (default is all remaining steps)")

	dbRootCmd.AddCommand(migrateCmd)
}
