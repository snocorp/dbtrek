package cmd

import (
	"database/sql"
	"fmt"
	"testing"

	"gitlab.com/snocorp/dbtrek/common"
	"gitlab.com/snocorp/dbtrek/migrations"
)

func TestMigrateCmd(t *testing.T) {
	for _, dbtype := range dbtypeList {
		t.Run(dbtype, func(t *testing.T) {
			dbname := getDbName()
			dbConfigURI := getDbConnectionURI(dbtype, dbname)
			drop, err := createDatabase(dbtype, dbname)
			defer drop()
			if err != nil {
				t.Fatal(err)
			}

			testDb, err = sql.Open(dbtype, dbConfigURI)
			if err != nil {
				t.Fatal(err)
				return
			}
			defer testDb.Close()

			testMigrator, err := newMigrator(dbtype, testDb)
			if err != nil {
				t.Fatal(err)
				return
			}
			err = testMigrator.CreateMigrationsTable()
			if err != nil {
				t.Error(err)
				return
			}

			t.Run("TestRunMigrations", func(t *testing.T) {
				testRunMigrations(t, testDb, testMigrator, dbtype, dbConfigURI)
			})
		})
	}
}

func testRunMigrations(t *testing.T, testDb *sql.DB, testMigrator migrations.Migrator, dbtype, dbConfigURI string) {
	err := executeCmd("db", "migrate",
		"--dbtype", dbtype,
		"--dbconfig", dbConfigURI,
		"--migrations", fmt.Sprintf("../examples/%v/sql.yaml", dbtype),
	)
	if err != nil {
		t.Error(err)
		return
	}

	expectMigrationsTable(t, testMigrator)

	expectOneRow(t, selectUsersTable(testDb, dbtype))
}

func selectUsersTable(testDb *sql.DB, dbtype string) func(handleRows common.RowHandler) error {
	return func(handleRows common.RowHandler) error {
		var query string
		if dbtype == "postgres" {
			query = `
SELECT 1 FROM
	information_schema.tables
WHERE
	table_name='users'
	AND
	table_type='BASE TABLE'
	AND
	table_catalog = current_database();`
		} else if dbtype == "mysql" {
			query = `
SELECT 1 FROM
	information_schema.tables
WHERE
	table_name='users'
	AND
	table_type='BASE TABLE'
	AND
	table_schema=database();`
		}
		rows, err := testDb.Query(query)
		if err != nil {
			return err
		}
		defer rows.Close()
		return handleRows(rows, err)
	}
}

func TestMigrateCmd_MissingMigrationFlag(t *testing.T) {
	for _, dbtype := range dbtypeList {
		t.Run(dbtype, func(t *testing.T) {
			dbname := getDbName()
			dbConfigURI := getDbConnectionURI(dbtype, dbname)
			err := executeCmd("db", "migrate",
				"--dbtype", dbtype,
				"--dbconfig", dbConfigURI,
			)
			if err == nil {
				t.Errorf("Expected an error but got none")
			} else if err.Error() != "migrations flag is required" {
				t.Error(err)
			}
		})
	}
}

func TestMigrateCmd_MissingDbconfigFlag(t *testing.T) {
	err := executeCmd("db", "migrate",
		"--migrations", "../examples/migrate.yaml",
	)
	if err == nil {
		t.Errorf("Expected an error but got none")
	} else if err.Error() != "dbconfig flag is required" {
		t.Error(err)
	}
}
