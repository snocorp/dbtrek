package cmd

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
)

var rollbackSteps int
var rollbackFile string

var rollbackCmd = &cobra.Command{
	Use:   "rollback",
	Short: "Rolls back a database schema migration",
	Long:  `Rolls back a database schema the given number of steps defined in the migraions file`,
	Run: func(cmd *cobra.Command, args []string) {
		if rollbackFile == "" {
			rollbackFile = os.Getenv(envTrekMigrations)
			if rollbackFile == "" {
				runtimeErr = errors.New("migrations flag is required")
				return
			}
		}

		runtimeErr = dbtrek.Rollback(rollbackFile, rollbackSteps)
	},
}

func init() {
	rollbackCmd.Flags().StringVarP(&rollbackFile, "migrations", "m", "", "(required) The yaml file containing the migrations")
	rollbackCmd.Flags().IntVarP(&rollbackSteps, "steps", "s", 1, "The number of steps to rollback")

	dbRootCmd.AddCommand(rollbackCmd)
}
