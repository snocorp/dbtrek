package cmd

import (
	"database/sql"
	"fmt"
	"testing"
)

func TestRollbackCmd(t *testing.T) {
	for _, dbtype := range dbtypeList {
		t.Run(dbtype, func(t *testing.T) {
			dbname := getDbName()
			dbConfigURI := getDbConnectionURI(dbtype, dbname)
			drop, err := createDatabase(dbtype, dbname)
			defer drop()
			if err != nil {
				t.Fatal(err)
			}

			testDb, err := sql.Open(dbtype, dbConfigURI)
			if err != nil {
				t.Fatal(err)
				return
			}
			defer testDb.Close()

			// first migrate the schema
			err = executeCmd("db", "migrate",
				"--dbtype", dbtype,
				"--dbconfig", dbConfigURI,
				"--migrations", fmt.Sprintf("../examples/%v/sql.yaml", dbtype),
			)
			if err != nil {
				t.Error(err)
				return
			}

			// ensure the migration worked
			expectOneRow(t, selectUsersTable(testDb, dbtype))

			// rollback
			err = executeCmd("db", "rollback",
				"--dbtype", dbtype,
				"--dbconfig", dbConfigURI,
				"--migrations", fmt.Sprintf("../examples/%v/sql.yaml", dbtype),
			)
			if err != nil {
				t.Error(err)
				return
			}

			// ensure the rollback worked
			expectNoRows(t, selectUsersTable(testDb, dbtype))
		})
	}
}

func TestRollbackCmd_MissingMigrationFlag(t *testing.T) {
	for _, dbtype := range dbtypeList {
		t.Run(dbtype, func(t *testing.T) {
			dbname := getDbName()
			dbConfigURI := getDbConnectionURI(dbtype, dbname)
			err := executeCmd("db", "rollback",
				"--dbtype", dbtype,
				"--dbconfig", dbConfigURI,
			)
			if err == nil {
				t.Errorf("Expected an error but got none")
			} else if err.Error() != "migrations flag is required" {
				t.Error(err)
			}
		})
	}
}

func TestRollbackCmd_MissingDbconfigFlag(t *testing.T) {
	err := executeCmd("db", "rollback",
		"--migrations", "../examples/postgres/sql.yaml",
	)
	if err == nil {
		t.Errorf("Expected an error but got none")
	} else if err.Error() != "dbconfig flag is required" {
		t.Error(err)
	}
}
