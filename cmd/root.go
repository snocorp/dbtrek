package cmd

import (
	"gitlab.com/snocorp/dbtrek/version"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Version:      "v" + version.VERSION,
	Use:          "dbtrek",
	Short:        "DBTrek is a schema migration tool",
	Long:         `DBTrek is a schema migration tool that is simple to use.`,
	SilenceUsage: true,
}
