package common

import "database/sql"

// RowHandler is a function that handles rows before the reference is closed
type RowHandler func(rows *sql.Rows, err error) error
