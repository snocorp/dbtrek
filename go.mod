module gitlab.com/snocorp/dbtrek

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lib/pq v1.3.0
	github.com/spf13/cobra v1.1.1
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gopkg.in/yaml.v2 v2.2.8
)

go 1.15
