package main

import (
	"gitlab.com/snocorp/dbtrek/cmd"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

func main() {
	cmd.Execute()
}
