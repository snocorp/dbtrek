package migrations

import (
	"database/sql"
	"io/ioutil"
	"log"
	"time"

	"gitlab.com/snocorp/dbtrek/common"

	yaml "gopkg.in/yaml.v2"
)

const (
	timestampLayout = "20060102150405"
)

type migrationLogic struct {
	Type  string `yaml:"type"`
	Value string `yaml:"value"`
}

type migration struct {
	Up       migrationLogic `yaml:"up"`
	Down     migrationLogic `yaml:"down"`
	Complete bool           `yaml:"-"`
}

// Migrations is a data structure to hold information about all the migrations
type Migrations struct {
	Migrations map[string]migration `yaml:"migrations"`
}

// Migrator is an interface for the database-related migration logic.
type Migrator interface {
	CreateMigrationsTable() error
	MarkMigrationComplete(id string) error
	MarkMigrationIncomplete(id string) error
	RunMigrationSQL(SQL string) error
	SelectMigrations(handleRows common.RowHandler) error
	SelectMigrationTable(handleRows common.RowHandler) error
}

// CreateMigrationsTable creates a table to track the migrations in the current schema
func CreateMigrationsTable(db *sql.DB) (err error) {
	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS dbtrek_migrations (
	id bigint PRIMARY KEY
)
`)

	return
}

// RunMigrationSQL runs the given SQL to execute a migration
func RunMigrationSQL(db *sql.DB, SQL string) (err error) {
	log.Printf("Executing SQL:\n%v", SQL)
	_, err = db.Exec(SQL)

	return
}

// AddMigration adds a new migration
func AddMigration(m *Migrations, upType, downType string) {
	timestamp := time.Now().UTC().Format(timestampLayout)
	m.Migrations[timestamp] = migration{
		Up: migrationLogic{
			Type: upType,
		},
		Down: migrationLogic{
			Type: downType,
		},
	}
}

// LoadMigrationFile loads the migrations from the given file
func LoadMigrationFile(migrateFile string) (m Migrations, err error) {
	log.Printf("Loading file %v", migrateFile)
	data, err := ioutil.ReadFile(migrateFile)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, &m)
	if err != nil {
		return
	}

	if m.Migrations == nil {
		m.Migrations = map[string]migration{}
	}

	return
}

// LoadMigrations loads the migrations from the given data and from the database
func LoadMigrations(migrator Migrator, m *Migrations) (err error) {
	log.Print("Loading migrations")
	err = migrator.SelectMigrations(func(rows *sql.Rows, err error) error {
		if err != nil {
			return err
		}

		for rows.Next() {
			var id string
			if err = rows.Scan(&id); err != nil {
				return err
			}

			migration, ok := m.Migrations[id]
			if ok {
				migration.Complete = true
				m.Migrations[id] = migration
			}
		}
		if err = rows.Err(); err != nil {
			return err
		}

		return nil
	})

	return
}

// SelectMigrations selects all the rows in the migrations table.
func SelectMigrations(db *sql.DB, handleRows common.RowHandler) error {
	rows, err := db.Query(`
SELECT id FROM dbtrek_migrations;
`)
	defer func() {
		if rows != nil {
			rows.Close()
		}
	}()
	return handleRows(rows, err)
}

// SelectMigrationTable selects the row containing information about the migration table.
func SelectMigrationTable(db *sql.DB, query string, handleRows common.RowHandler) error {
	rows, err := db.Query(query)
	defer func() {
		if rows != nil {
			rows.Close()
		}
	}()
	return handleRows(rows, err)
}
