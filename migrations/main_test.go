package migrations

import (
	"database/sql"
	"strings"
	"testing"
	"time"

	"gitlab.com/snocorp/dbtrek/common"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	yaml "gopkg.in/yaml.v2"
)

const testMigrations = `
migrations:
  20180910220000:
    up:
      type: sql
      value: |
        CREATE TABLE users
        (
            id bigint NOT NULL,
            email character varying(255) NOT NULL,
            PRIMARY KEY (id)
        );
    down:
      type: sql
      value: |
        DROP TABLE users;
`

func TestMigrationEncoding(t *testing.T) {
	m := Migrations{
		Migrations: map[string]migration{
			"20180910220000": migration{
				Up:   migrationLogic{Type: "sql", Value: "-- up sql"},
				Down: migrationLogic{Type: "sql", Value: "-- down sql"},
			},
		},
	}

	_, err := yaml.Marshal(m)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestAddMigration(t *testing.T) {
	m := Migrations{
		Migrations: map[string]migration{},
	}
	AddMigration(&m, "up", "down")

	if len(m.Migrations) != 1 {
		t.Errorf("Expected 1 migration but got %v", len(m.Migrations))
		return
	}
	for k, v := range m.Migrations {
		timestamp, err := time.Parse(timestampLayout, k)
		if err != nil {
			t.Error(err)
		}

		now := time.Now()
		if timestamp.Add(time.Second * 10).Before(now) {
			t.Errorf("Expected timestamp to be less than 10s before current time but got %v", now.Sub(timestamp))
			return
		}

		if v.Up.Type != "up" {
			t.Errorf("Expected up type to be 'up' but got '%v'", v.Up.Type)
		}
		if v.Down.Type != "down" {
			t.Errorf("Expected down type to be 'down' but got '%v'", v.Down.Type)
		}
	}
}

func TestCreateMigrationsTable(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	mock.ExpectExec(`CREATE TABLE IF NOT EXISTS dbtrek_migrations`).WillReturnResult(sqlmock.NewResult(1, 1))

	err = CreateMigrationsTable(db)
	if err != nil {
		t.Error(err)
	}
}

func TestLoadMigrations(t *testing.T) {
	var migrator Migrator
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	migrator = &mockMigrator{db}

	mock.ExpectQuery("SELECT id FROM dbtrek_migrations").WillReturnRows(
		sqlmock.NewRows([]string{"id"}).AddRow("20180910220000"),
	)

	var m Migrations
	err = yaml.Unmarshal([]byte(testMigrations), &m)
	if err != nil {
		t.Error(err)
		return
	}

	err = LoadMigrations(migrator, &m)
	if err != nil {
		t.Error(err)
		return
	}

	migration, ok := m.Migrations["20180910220000"]
	if !ok {
		t.Error("Expected migrations to include id 20180910220000")
	}

	if !migration.Complete {
		t.Error("Expected migration to be complete")
	}
	upSQL := migration.Up.Value
	if !strings.Contains(upSQL, "CREATE TABLE users") {
		t.Errorf("Up SQL did not match expectations\n%v", upSQL)
	}
	downSQL := migration.Down.Value
	if !strings.Contains(downSQL, "DROP TABLE users") {
		t.Errorf("Down SQL did not match expectations\n%v", downSQL)
	}
}

type mockMigrator struct {
	db *sql.DB
}

func (m *mockMigrator) CreateMigrationsTable() (err error) {
	return nil
}

func (m *mockMigrator) RunMigrationSQL(SQL string) error {
	return nil
}

func (m *mockMigrator) MarkMigrationComplete(id string) error {
	return nil
}

func (m *mockMigrator) MarkMigrationIncomplete(id string) error {
	return nil
}

func (m *mockMigrator) SelectMigrations(handleRows common.RowHandler) error {
	return SelectMigrations(m.db, handleRows)
}

func (m *mockMigrator) SelectMigrationTable(handleRows common.RowHandler) error {
	return nil
}
