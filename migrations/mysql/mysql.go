package mysql

import (
	"database/sql"

	"gitlab.com/snocorp/dbtrek/common"
	"gitlab.com/snocorp/dbtrek/migrations"
)

type mysqlMigrator struct {
	db *sql.DB
}

// NewMigrator returns a new MySQL migrator instance.
func NewMigrator(db *sql.DB) migrations.Migrator {
	return &mysqlMigrator{
		db: db,
	}
}

func (m *mysqlMigrator) CreateMigrationsTable() (err error) {
	return migrations.CreateMigrationsTable(m.db)
}

func (m *mysqlMigrator) RunMigrationSQL(SQL string) error {
	return migrations.RunMigrationSQL(m.db, SQL)
}

func (m *mysqlMigrator) MarkMigrationComplete(id string) (err error) {
	// insert the id into the dbtrek_migrations table
	_, err = m.db.Exec(`
		INSERT INTO dbtrek_migrations (id) VALUES (?);
		`, id)

	return
}

func (m *mysqlMigrator) MarkMigrationIncomplete(id string) (err error) {
	// remove the id from the dbtrek_migrations table
	_, err = m.db.Exec(`
		DELETE FROM dbtrek_migrations WHERE id = ?;
		`, id)

	return
}

func (m *mysqlMigrator) SelectMigrations(handleRows common.RowHandler) error {
	return migrations.SelectMigrations(m.db, handleRows)
}

func (m *mysqlMigrator) SelectMigrationTable(handleRows common.RowHandler) error {
	return migrations.SelectMigrationTable(m.db, `
SELECT 1 FROM
	information_schema.tables
WHERE
	table_name='dbtrek_migrations'
	AND
	table_type='BASE TABLE'
	AND
	table_schema=database()
	;`, handleRows)
}
