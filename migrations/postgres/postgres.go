package postgres

import (
	"database/sql"

	"gitlab.com/snocorp/dbtrek/common"
	"gitlab.com/snocorp/dbtrek/migrations"
)

type postgresMigrator struct {
	db *sql.DB
}

// NewMigrator returns a new Postgres migrator instance.
func NewMigrator(db *sql.DB) migrations.Migrator {
	return &postgresMigrator{
		db: db,
	}
}

func (m *postgresMigrator) CreateMigrationsTable() (err error) {
	return migrations.CreateMigrationsTable(m.db)
}

func (m *postgresMigrator) RunMigrationSQL(SQL string) error {
	return migrations.RunMigrationSQL(m.db, SQL)
}

func (m *postgresMigrator) MarkMigrationComplete(id string) (err error) {
	// insert the id into the dbtrek_migrations table
	_, err = m.db.Exec(`
		INSERT INTO dbtrek_migrations (id) VALUES ($1);
		`, id)

	return
}

func (m *postgresMigrator) MarkMigrationIncomplete(id string) (err error) {
	// remove the id from the dbtrek_migrations table
	_, err = m.db.Exec(`
		DELETE FROM dbtrek_migrations WHERE id = $1;
		`, id)

	return
}

func (m *postgresMigrator) SelectMigrations(handleRows common.RowHandler) error {
	return migrations.SelectMigrations(m.db, handleRows)
}

func (m *postgresMigrator) SelectMigrationTable(handleRows common.RowHandler) error {
	return migrations.SelectMigrationTable(m.db, `
SELECT 1 FROM
	information_schema.tables
WHERE
	table_name='dbtrek_migrations'
	AND
	table_type='BASE TABLE'
	AND
	table_catalog = current_database()
	;`, handleRows)
}
