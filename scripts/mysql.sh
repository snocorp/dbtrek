#!/bin/bash

# run percona in a docker container, setting a default password and mapping the default port
docker run --rm -it \
    --name mysql \
    -p "127.0.0.1:3306:3306" \
    -e MYSQL_ROOT_PASSWORD=dbtrek \
    -e MYSQL_DATABASE=dbtrek \
    percona
