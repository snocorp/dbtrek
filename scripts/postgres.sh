#!/bin/bash

# run postres in a docker container, setting a default password and mapping the default port
docker run --rm -it \
    --name postgres \
    -p "127.0.0.1:5432:5432" \
    -e POSTGRES_USER=root \
    -e POSTGRES_PASSWORD=dbtrek \
    postgres
